from django.urls import path
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    path('', TemplateView.as_view(template_name="main/index.html"),  name='index'),
    path('register/', views.register, name='register'),
    path('creators/', views.profile_list, name='profile_list'),
    path('edit/', views.ProfileUpdate.as_view(), name='edit'),
    path('delete/<int:pk>', views.DeleteAddress.as_view(), name='delete'),
    path('<str:username>/', views.profile, name='profile'),
    path('<str:username>/report/', views.ReportView.as_view(), name='report'),
    path('<str:username>/request/', views.RequestView.as_view(), name='request'),
]
