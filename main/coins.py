import json
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent


def coin_list():
    f = open(BASE_DIR / "main/manifest.json", 'r')
    data = json.loads(f.read())
    coins = []

    for coin in data:
        coins.append(coin['symbol'])

    return coins


def coin_dict():
    f = open("manifest.json", 'r')
    data = json.loads(f.read())
    coins = {}

    for coin in data:
        coins[coin['symbol']] = coin['name']

    return coins
