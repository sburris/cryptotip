from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, redirect, render, reverse
from django.views.generic import CreateView, UpdateView, DeleteView
from django.contrib import messages

from .coins import coin_list
from .forms import RegisterForm, AddForm
from .models import Address, Profile, Report, Request


def register(request):
    """The view for creating new accounts"""
    context = {
        'form': RegisterForm(),
    }
    return render(request, 'main/register.html', context)


def profile_list(request):
    context = {
        'profiles': Profile.objects.filter(listed=True),
    }
    return render(request, 'main/profile_list.html', context)


def profile(request, username):
    user = get_object_or_404(User, username=username)

    if request.method == "POST":
        form = AddForm(request.POST)
        if form.is_valid():
            newcoin = Address.objects.create(owner=user, coin=form.cleaned_data['coin'], address=form.cleaned_data['address'])
            messages.info(request, 'New {} addresses added successfully.'.format(newcoin.coin.upper()))
            return redirect(reverse('profile', args=[user]))
        else:
            messages.info(request, 'Error occurred while adding new address.')
            return redirect(reverse('profile', args=[user]))

    elif request.method == "GET":
        """View for each creator's profile"""
        profile, created = Profile.objects.get_or_create(owner=user)
        form = AddForm()

        coins = coin_list()

        context = {
            'user': user,
            'addresses': Address.objects.filter(owner=user.id).order_by('coin'),
            'profile': profile,
            'coins': coins,
            'form': form,
        }
        return render(request, 'main/profile.html', context)


class ReportView(CreateView):
    model = Report
    fields = ["message"]

    def get_context_data(self, **kwargs):
        ctx = super(ReportView, self).get_context_data(**kwargs)
        ctx['user'] = get_object_or_404(User, username=self.kwargs['username'])
        return ctx

    def form_valid(self, form):
        report = form.save(commit=False)
        report.user = get_object_or_404(User, username=self.kwargs['username'])
        return super(ReportView, self).form_valid(form)


class RequestView(CreateView):
    model = Request
    fields = ["coin"]

    def get_context_data(self, **kwargs):
        ctx = super(RequestView, self).get_context_data(**kwargs)
        ctx['user'] = get_object_or_404(User, username=self.kwargs['username'])
        return ctx

    def form_valid(self, form, **kwargs):
        report = form.save(commit=False)
        report.user = get_object_or_404(User, username=self.kwargs['username'])
        return super(RequestView, self).form_valid(form)


class ProfileUpdate(LoginRequiredMixin, UpdateView):
    model = Profile
    fields = ["desc", "bio", "listed"]

    def get_object(self):
        profile, created = Profile.objects.get_or_create(owner=self.request.user)
        return profile


class DeleteAddress(DeleteView):
    model = Address

    def get_success_url(self):
        messages.info(self.request, 'Address deleted successfully.')
        return reverse('profile', args=[self.request.user])

    def get_context_data(self, **kwargs):
        ctx = super(DeleteView, self).get_context_data(**kwargs)
        ctx['coins'] = coin_list()
        return ctx
