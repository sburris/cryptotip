from django.contrib.auth.models import User
from django.forms import ModelForm, TextInput

from .models import Address


class RegisterForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password']


class AddForm(ModelForm):
    class Meta:
        model = Address
        fields = ['coin', 'address']
        widgets = {
            'coin': TextInput(attrs={'list': 'coinlist', 'placeholder': 'COIN ABBREVIATION', 'autocomplete': 'off'}),
            'address': TextInput(attrs={'placeholder': 'ADDRESS'}),
        }
