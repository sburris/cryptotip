from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


# Create your models here.
class Profile(models.Model):
    bio = models.TextField(max_length=256)
    desc = models.CharField(max_length=64)
    listed = models.BooleanField(default=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "{}'s profile".format(self.owner.username)

    def get_absolute_url(self):
        return reverse("profile", args=[self.owner.username])


class Address(models.Model):
    coin = models.CharField(max_length=8)
    address = models.CharField(max_length=128)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "{}'s {}: {}".format(self.owner, self.coin, self.address)


class Request(models.Model):
    coin = models.CharField(max_length=32)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    read = models.BooleanField(default=True)

    def get_absolute_url(self):
        return reverse("profile", args=[self.user.username])

    def __str__(self):
        return self.user.username + ': ' + self.coin


class Report(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_open = models.BooleanField(default=True)
    message = models.TextField(max_length=256)

    def get_absolute_url(self):
        return reverse("profile", args=[self.user.username])

    def __str__(self):
        if self.is_open:
            return 'OPEN: ' + self.user.username
        else:
            return self.user.username
